.\"
.\" Copyright (c) 2013 ... 2024 2025
.\"     John McCue
.\"
.\" Permission to use, copy, modify, and distribute this software
.\" for any purpose with or without fee is hereby granted,
.\" provided that the above copyright notice and this permission
.\" notice appear in all copies.
.\"
.\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
.\" WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
.\" WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
.\" CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING
.\" FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
.\" CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
.\" OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
.\" SOFTWARE.
.\"
.TH JMERGE 1 "2013-01-04" "JMC" "User Commands"
.SH NAME
jmerge - Creates a merged file from 2 or more files.
.SH SYNOPSIS
jmerge [OPTIONS] [FILE...]
.SH DESCRIPTION
Merge data records from a Data File with one unique 'key'
with 1 or more Data Files.
Matched Data from the Data File will be appended to each
Key File Record, creating an Output File.
.PP
Requires at least 2 input files.
One file is a 'Key File' and all other files
are data that will be appended to each record of the
Key File.
This results in a file with Records combined based
upon the value in the first column of each
file.
.PP
Column 1 of each file is used to match data.
.TP
-d delm
Optional, Fields are delimited by char 'delm'.
If not specified the default is to use '|' as the delimiter.
For example to use:
.nf
    delimiter  Use arg
    ---------  -------------------
       |       -d '|'  OR -d 124
       TAB     -d 9
       ^G      -d 7
.fi
.TP
-e file
Optional, if used, write error messages to file 'file'.
If not specified, errors written to stderr.
.TP
-F
Optional, the first record of each file
is a heading record.
This means the first record on all files
will be ignored.
.TP
-f
Optional, Force file create.
Create file even if the target file exists.
.TP
-h
Show brief help and exit.
.TP
-i file
Required, this identifies the main driver file.
All records must be unique by 'key'.
.TP
-K file
Optional, if used, write counts of matches
found on Key File (see -i).
If not specified, file is not created.
.TP
-o file
Optional, if used, write output to file 'file'.
If not specified, output written to stdout.
.TP
-r n
Optional, after 'n' Reads, pause for
the specified milliseconds (see -s below).
If not specified default to 100 reads.
Ignored unless '-s' is specified.
.TP
-s n
Optional, if used, after the specified number of Records
are processed, sleep for 'n' milliseconds (see -r above).
If not specified, do not sleep (pause) after reading
a certain number of records (see -r).
This will help prevent overheating on a Laptop
that is susceptible to heat.
But, it will showdown processing.
.TP
-u
Optional, Unsorted.
All Files are NOT sorted by Key.
Default, all files are sorted by data in the First Column.
.TP
-V
Output version information and exit.
.TP
-v
Optional, Verbose Level.
Print information about the run,
default do not show run messages.
Can be specified multiple times,
each specification increases verbose level:
.nf
    Level  Meaning
    -----  -------------------------------------
    = 0    Show only Error Messages
    >= 1   Above plus print show headings on
           Files created.
           This heading will show the source
           Data File.
    >= 2   Show summary and warnings on stderr
.fi
.SH EXAMPLE
The command will combine key_file.txt and data_file.txt
printing output on stdout.
File stat.txt will also be created showing Record
Match Counts.
.nf

Command:
   % jmerge -d '|' -i key_file.txt -K stat.txt data_file.txt

Results:

If this is your Input Key File (-i key_file.txt):
    001|A
    002|B
    003|C
    005|E
.fi
.nf

And this is the Input Data File (data_file.txt):
    001|AA|B12
    001|AB|B345
    003|CA|B5
    003|CB|B67
    004|DA|B67
    005|EA|B89
    005|EB|B100
    005|EC|B104
.fi
.nf

This is what the combined Output File will look like:
    MATCHED:  001|A|001|AA|B12
    MATCHED:  001|A|001|AB|B345
    MATCHED:  003|C|003|CA|B5
    MATCHED:  003|C|003|CB|B67
    NO MATCH: 004|DA|B67
    MATCHED:  005|E|005|EA|B89
    MATCHED:  005|E|005|EB|B100
    MATCHED:  005|E|005|EC|B104
Note, 'NO MATCH' just prints data from the Data File.
.fi
.nf

And this is what the Optional Output Key (-K stat.txt)
Status File will look like.
The first column shows the count of Matches,
the second column shows the Key.
    2 001
    0 002
    2 003
    3 005
.fi
.SH DIAGNOSTICS
A 'key' is the first column on each File.
.PP
If you specify '-e', in some cases info will still be
written to stderr.
This can occur during Command Line Argument Processing.
.PP
Files read are locked using flock(2),
but some processes may ignore that lock.
This is done as an attempt to prevent other processes
from updating the files being processed.
.PP
Best to sort all input files by
the first column.
Otherwise runtime could be significant (see -u),
depending upon the size of the Files.
.PP
When processing unsorted files,
if the key file (-i) has duplicate records
the program will not fail.
Just the duplicates will be ignored.
This is opposed to processing Sorted Files.
The Key File must be unique by the First Column
when processing Sorted Files (the default).
.PP
When processing more than one data file,
data of the second file is just appended
to the key file.
If you want to append data records from
two or more files like this:
.nf
    Key-File-Data|file-1-data|file-2-data
.fi
First combine files 1 and 2 into a new file using
this program, then combine with the key file.
.SH SEE-ALSO
awk(1),
cut(1),
flock(2),
jcsv(local),
jr(local),
paste(1),
sed(1)
.SH ERROR-CODES
.nf
0 success
1 processing error or help/rev displayed
.fi
